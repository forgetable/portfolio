import React, { Component } from 'react'
import './normalize.css'

import Header from './Components/Header'

class App extends Component {
  render() {
    return (
      <div>
        <Header />
      </div>
    );
  }
}

export default App;
