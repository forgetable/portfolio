import styles from './css/index'
import { css } from 'aphrodite/no-important'

export default { css, styles }
export { styles }
export { css }
