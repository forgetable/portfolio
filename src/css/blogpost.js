import { StyleSheet } from 'aphrodite/no-important'

export default StyleSheet.create({
  main: {
    boxSizing: 'border-box',
    overflow: 'hidden',
    lineHeight: '1.3em',
    width: '100%',
    height: 235,
    background: '#fff',
    padding: '20px 20px 20px 260px',
    marginBottom: 33,
    position: 'relative',
    borderRadius: 2,
    boxShadow: '3px 3px 30px 0 rgba(0,0,0,.1)',
    cursor: 'pointer',
    ':hover aside[gradient]::before': {
      opacity: .9,
    },
    ':hover aside[gradient]': {
      // transform: 'translateX(-10px)'
    },
    ':hover button::after': {
      opacity: 1
    }
  },
  skewDecor: {
    backgroundImage: 'linear-gradient(115deg, #1A345F, #741B40)',
    position: 'absolute',
    top: 0,
    left: 80,
    width: 'calc(100% - 280px)',
    transform: 'skew(-25deg) translateX(255px)',
    userSelect: 'none',
    pointerEvents: 'none',
    height: '100%',
    zIndex: 1
  },
  image: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: 385,
    height: 235,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    filter: 'blur(50px)',
    transform: 'translateX(50px)',
    clipPath: 'polygon(0 0, 100% 0, 71.5% 100%, 0% 100%)',
  },
  imageNotBlurred: {
    filter: 'none',
    transform: 'none',
    width: 335,
    clipPath: 'polygon(0 0, 100% 0, 67.5% 100%, 0% 100%)',
    transition: 'transform .4s ease',
    ':before': {
      content: '""',
      position: 'absolute',
      left: 0,
      top: 0,
      right: 0,
      bottom: 0,
      backgroundImage: 'linear-gradient(-115deg, #72ffbb, #67defc)',
      opacity: 0,
      transition: 'opacity .2s ease'
    }
  },
  contents: {
    position: 'relative',
    height: '100%',
    zIndex: 2
  },
  heading: {
    display: 'inline',
    background: '#fff',
    color: '#5DA5CF',
    lineHeight: 1.4,
    boxShadow: '1px 0 0 5px #fff',
    fontWeight: 400
  },
  description: {
    background: '#fff',
    display: 'inline',
    boxShadow: '0 0 0px 4px #fff',
    marginTop: 20,
    color: '#454545',
    lineHeight: 1.4
  },
  readMore: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    background: 'none',
    border: 'none',
    color: '#fff',
    outline: 'none',
    cursor: 'pointer',
    ':after': {
      content: '""',
      position: 'absolute',
      top: '100%',
      left: 0,
      right: 0,
      height: 1,
      background: '#fff',
      opacity: 0,
      transition: 'opacity .1s ease'
    }
  }
})
