import { StyleSheet } from 'aphrodite/no-important'

export default StyleSheet.create({
  main: {
    display: 'inline-block',
  },
  logo: {
    height: 100,
    top: 27,
    left: 27,
    position: 'relative',
    cursor: 'pointer'
  },
  decor1: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: 310,
    height: 500,
    background: '#e9e9e9',
    clipPath: 'polygon(0 0, 100% 0, 0 100%, 0 100%)',
    opacity: .5
  },
  decor2: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: 500,
    height: 310,
    background: '#e9e9e9',
    clipPath: 'polygon(0 0, 100% 0, 0 100%, 0 100%)',
    opacity: .5
  },
  menu: {
    position: 'absolute',
    left: 310,
    top: 54,
    width: 30,
    height: 20,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    cursor: 'pointer',
    '> div': {
      height: 4,
      width: '100%',
      background: '#454545',
      borderRadius: 2
    }
  }
})