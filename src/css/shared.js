import { StyleSheet } from 'aphrodite/no-important'

export default StyleSheet.create({
  heading: {
    fontFamily: '"Ubuntu", sans-serif',
    fontWeight: 700
  },
  article: {
    lineHeight: '1.3em'
  }
  
})