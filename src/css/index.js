import shared from './shared'
import containers from './containers'
import header from './header'
import promo from './promo'
import blogpost from './blogpost'

import home from './pages/home'
import blog from './pages/blog'

export default { shared, containers, header, promo, home, blog, blogpost }