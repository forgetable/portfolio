import { StyleSheet } from 'aphrodite/no-important'

export default StyleSheet.create({
  main: {
    height: 100000,
    maxHeight: 300,
    background: '#333',
    position: 'relative',
    overflow: 'hidden',
    marginBottom: 55
  },
  bg: {
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0
  }
  
})