import { StyleSheet } from 'aphrodite/no-important'

export default StyleSheet.create({
  main: {
    margin: '0 auto',
    padding: '0 50px'
  },
  heading: {
    textTransform: 'uppercase',
    fontSize: 30
  }
})