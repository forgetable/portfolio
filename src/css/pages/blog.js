import { StyleSheet } from 'aphrodite/no-important'

export default StyleSheet.create({
  main: {
    margin: '0 auto',
    padding: '0 50px',
    paddingBottom: 75
  },
  heading: {
    textTransform: 'uppercase',
    fontSize: 30
  }
})