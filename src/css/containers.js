import { StyleSheet } from 'aphrodite/no-important'

const container = { width: '100%' }

export default StyleSheet.create({
  main: {
    ...container,
    maxWidth: 775
  },
  big: {
    ...container,
    maxWidth: 1300
  }
})