export default {
  headerPaths: [
    {
      title: 'Главная',
      to: '/'
    },
    {
      title: 'Блог',
      to: '/blog'
    },
    {
      title: 'Обзоры',
      to: '/reviews'
    },
    {
      title: 'Джем',
      to: '/jam'
    }
  ]
}