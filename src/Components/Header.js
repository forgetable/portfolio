import React, { Component } from 'react'
import { styles, css } from '../workspace'

import logo from '../images/logo.png'


class Header extends Component {

  render() {
    return (
      <div className={css(styles.header.main)}>
        <div className={css(styles.header.decor1)} />
        <div className={css(styles.header.decor2)} />
        <img className={css(styles.header.logo)} src={logo} alt="Logo"/>
        <div className={css(styles.header.menu)}>
          <div className={css(styles.header.menuPiece)} />
          <div className={css(styles.header.menuPiece)} />
          <div className={css(styles.header.menuPiece)} />
        </div>
      </div>
    )
  }
}

export default Header