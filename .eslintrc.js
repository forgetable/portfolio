module.exports = {
  'env': {
    'browser': true,
    'es6': true
  },
  plugins: ['react', 'eslint-plugin-flowtype'],
  parser: 'babel-eslint',
  'parserOptions': {
    'ecmaVersion': 8,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    }
  },
  'extends': ['eslint:recommended', 'plugin:react/recommended'],
  'rules': {
    'indent': [
      'error',
      2,
      {
        'FunctionDeclaration': {
          'parameters': 'first'
        },
        'FunctionExpression': {
          'parameters': 'first'
        },
        'CallExpression': {
          'arguments': 'first'
        }
      }
    ],
    'quotes': [
      'error',
      'single'
    ],
    semi: [
      'error',
      'never'
    ],
    'no-console': 1,
    'react/no-unescaped-entities': 0,
    'react/prop-types': 0,
    'react/jsx-fragments': ['none', 'qwe'],
    'no-unused-vars': 1,
    "flowtype/boolean-style": [
      2,
      "boolean"
    ],
    "flowtype/define-flow-type": 1,
    "flowtype/delimiter-dangle": [
      2,
      "never"
    ],
    "flowtype/generic-spacing": [
      2,
      "never"
    ],
    "flowtype/no-primitive-constructor-types": 2,
    "flowtype/no-types-missing-file-annotation": 2,
    "flowtype/no-weak-types": 2,
    "flowtype/object-type-delimiter": [
      2,
      "comma"
    ],
    "flowtype/require-parameter-type": 2,
    "flowtype/require-return-type": [
      2,
      "always",
      {
        "annotateUndefined": "never"
      }
    ],
    "flowtype/require-valid-file-annotation": 2,
    "flowtype/semi": [
      2,
      "always"
    ],
    "flowtype/space-after-type-colon": [
      2,
      "always"
    ],
    "flowtype/space-before-generic-bracket": [
      2,
      "never"
    ],
    "flowtype/space-before-type-colon": [
      2,
      "never"
    ],
    "flowtype/type-id-match": [
      2,
      "^([A-Z][a-z0-9]+)+Type$"
    ],
    "flowtype/union-intersection-spacing": [
      2,
      "always"
    ],
    "flowtype/use-flow-type": 1,
    "flowtype/valid-syntax": 1,
  },
  settings: {
    flowType: { onlyFilesWithFlowAnnotation: false }
  }
};
